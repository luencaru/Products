var config = require("../../shared/config");
var fetchModule = require("fetch");
var ObservableArray = require("data/observable-array").ObservableArray;
var valueList = require("nativescript-drop-down");

function CategoryListViewModel(items) {
  var baseUrl = config.apiUrl + "categories/";
  var viewModel = new valueList.ValueList([]);

  viewModel.load = function() {
    return fetch(baseUrl, {
        headers: getCommonHeaders()
      })
      .then(handleErrors)
      .then(function(response) {
        return response.json();
      }).then(function(data) {
        data.forEach(function(category) {
          viewModel.push({
            value: category.id.toString(),
            display: category.name
          });
        });
      });
  };

  viewModel.empty = function() {
    while (viewModel.length) {
      viewModel.pop();
    }
  };

  return viewModel;
}

function getCommonHeaders() {
  return {
    "Content-Type": "application/json",
  }
}

function handleErrors(response) {
  if (!response.ok) {
    console.log(JSON.stringify(response));
    throw Error(response.statusText);
  }
  return response;
}

module.exports = CategoryListViewModel