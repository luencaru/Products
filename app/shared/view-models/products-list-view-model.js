var config = require("../../shared/config");
var fetchModule = require("fetch");
var ObservableArray = require("data/observable-array").ObservableArray;
var valueList = require("nativescript-drop-down");

function ProductListViewModel(items) {
  var baseUrl = config.apiUrl + "categories/brands/families/";
  var productModel = new ObservableArray(items);

  productModel.load = function(selectedFamilyIndex) {
    return fetch(baseUrl + selectedFamilyIndex + "/products/", {
        headers: getCommonHeaders()
      })
      .then(handleErrors)
      .then(function(response) {
        return response.json();
      }).then(function(data) {
        data.forEach(function(product) {
          productModel.push({
            value: product.id.toString(),
            display: product.name
          });
        });
      });
  };

  productModel.empty = function() {
    while (productModel.length) {
      productModel.pop();
    }
  };

  return productModel;
}

function getCommonHeaders() {
  return {
    "Content-Type": "application/json",
  }
}

function handleErrors(response) {
  if (!response.ok) {
    console.log(JSON.stringify(response));
    throw Error(response.statusText);
  }
  return response;
}

module.exports = ProductListViewModel