var observableModule = require("data/observable");
var ProductListViewModel = require("../../shared/view-models/products-list-view-model");
var frameModule = require("ui/frame");

var page;

var productList = new ProductListViewModel([]);
var pageData = observableModule.fromObject({
  productList: productList,
  grocery: ""
});

exports.productLoaded = function(args) {
  page = args.object;
  var selectedFamilyIndex = page.navigationContext.selectedFamilyIndex;
  var listView = page.getViewById("productList");
  page.bindingContext = pageData;
  pageData.set("isLoading", true);
  productList.empty();
  productList.load(selectedFamilyIndex).then(function() {
    pageData.set("isLoading", false)
    listView.animate({
      opacity: 1,
      duration: 1000
    });
  });
};

exports.OnBack = function() {
  var topmost = frameModule.topmost();
  topmost.goBack();
}