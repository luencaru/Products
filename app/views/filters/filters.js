var observable = require("data/observable");
var observableArray = require("data/observable-array");
var pages = require("ui/page");
var CategoryListViewModel = require("../../shared/view-models/filters-view-model");
var config = require("../../shared/config");
var frameModule = require("ui/frame");

var valueList = require("nativescript-drop-down");
var requiredColor = "rgba(248, 42, 42, 0.38)"
var norequiredColor = "#BDBDBD"
var page;
var selectedFamilyIndex = null;
var categoryList = new CategoryListViewModel([]);
var brandList = new valueList.ValueList([]);
var familyList = new valueList.ValueList([]);

var pageData = observable.fromObject({
  categoryRequired: requiredColor,
  brandRequired: norequiredColor,
  familyRequired: norequiredColor,
  categoryList: categoryList,
  selectedCategoryIndex: null,
  categoryEnabled: false,
  brandList: brandList,
  selectedBrandIndex: null,
  brandEnabled: false,
  familyList: familyList,
  selectedFamilyIndex: null,
  familyEnabled: false,
  searchEnabled: false
});


exports.dataLoaded = function(args) {
  page = args.object;
  var categoryFilter = page.getViewById("categoryFilter");

  page.bindingContext = pageData;
  pageData.set("isLoading", true);
  if (pageData.categoryList.length == 0) {
    categoryList.load().then(function() {
      pageData.categoryEnabled = true;
    });
  }
};

exports.SelectedCategoryIndexChanged = function(args) {
  newCategory = categoryList.getValue(args.newIndex);
  pageData.selectedBrandIndex = null;
  pageData.brandList.length = 0;
  pageData.familyEnabled = false;
  pageData.selectedFamilyIndex = null;
  pageData.familyList.length = 0;
  pageData.searchEnabled = false;
  if (newCategory != null) {
    pageData.categoryRequired = norequiredColor;
    return fetch(config.apiUrl + "categories/" + newCategory + "/brands/", {
        headers: getCommonHeaders()
      })
      .then(handleErrors)
      .then(function(response) {
        return response.json();
      }).then(function(data) {
        data.forEach(function(brand) {
          pageData.brandList.push({
            value: brand.id.toString(),
            display: brand.name
          });
        });
        pageData.brandEnabled = true;
        pageData.brandRequired = requiredColor;
      });

  }
};

exports.SelectedBrandIndexChanged = function(args) {
  newBrand = brandList.getValue(args.newIndex);
  pageData.selectedFamilyIndex = null;
  pageData.familyList.length = 0;
  pageData.searchEnabled = false;
  if (newBrand != null) {
    return fetch(config.apiUrl + "categories/brands/" + newBrand + "/families/", {
        headers: getCommonHeaders()
      })
      .then(handleErrors)
      .then(function(response) {
        return response.json();
      }).then(function(data) {
        data.forEach(function(family) {
          pageData.familyList.push({
            value: family.id.toString(),
            display: family.name
          });
        });
        pageData.brandRequired = norequiredColor;
        pageData.familyEnabled = true;
        pageData.familyRequired = requiredColor;
      });

  }
};

exports.SelectedFamilyIndexChanged = function(args) {
  selectedFamilyIndex = familyList.getValue(args.newIndex);
  if (selectedFamilyIndex != null) {
    pageData.searchEnabled = true;
    pageData.familyRequired = norequiredColor;
  }
};

exports.OnReset = function() {
  pageData.selectedCategoryIndex = null;
  pageData.selectedBrandIndex = null;
  pageData.brandList.length = 0;
  pageData.brandEnabled = false;
  pageData.familyEnabled = false;
  pageData.selectedFamilyIndex = null;
  pageData.familyList.length = 0;
  pageData.searchEnabled = false;
  pageData.categoryRequired = requiredColor;
  pageData.brandRequired = norequiredColor;
  pageData.familyRequired = norequiredColor;
};

exports.OnSubmit = function() {
  var topmost = frameModule.topmost();
  topmost.navigate({
    moduleName: "views/list/list",
    context: {
      selectedFamilyIndex: selectedFamilyIndex
    },
    animated: true
  });
};


function getCommonHeaders() {
  return {
    "Content-Type": "application/json",
  }
};

function handleErrors(response) {
  if (!response.ok) {
    console.log(JSON.stringify(response));
    throw Error(response.statusText);
  }
  return response;
};